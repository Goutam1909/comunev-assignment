import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  form: FormGroup;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      first_name: ['', Validators.required],
      last_name: [''],
      username: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(8)]],
      date_of_birth: ['', Validators.required],
      country: ['', Validators.required]
    });
  }

  submitForm() {
    if (this.form.valid) {
      this.submitted = true;
      setTimeout(() => {
        this.router.navigate(['/success']);
      }, 1000);
    } else {
      this.form.markAllAsTouched();
    }
  }
}
